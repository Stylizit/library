// IIFE - Immediately Invoked Function Expression
(function(_fct){
    // The global jQuery object is passed as a parameter
  	_fct(window.jQuery, window, document);

}(function($, window, document) {

// The $ is now locally scoped 
// Listen for the jQuery ready event on the document
  $(function(){
    
    // The DOM is ready!
    console.log("DOM is ready");

    // Only call functions we need when DOM is ready
    var waiting = 1200;
    var duration = 400;
    slide(duration, waiting);

    //////////////
    // TODO :
    // - Add auto animation
    // - Set correct timeout (related with prev)
    /////////////

  });

  // DOM isn't ready yet !
  console.log("DOM isn't ready");

  function slide(duration, waiting){

    // Caching variables
    var target = $('.expandable-img-slider');
    var size = $("li", target).length;
    var width = $("li", target).width();
    var height = $("li", target).height();
    var clickable = true;

    // Anim variables 
    var ts = size - 1;
    var t = 0;

    target.width(width);
    target.height(height);
    $("ul", target).css("width", size * width);

    // Implement continuous navigation
    $("ul", target).prepend($("ul li:last-child", target).clone().css("margin-left","-"+ width +"px"));
    $("ul", target).append($("ul li:nth-child(2)", target).clone());
    $("ul", target).css('width',(size + 1) * width);

    console.log("largeur ul: "+size*width+", nombre li: "+size);

    // Create slider navigation links
    for(var i = 0; i < size; i++){           
      $(document.createElement("li"))
          .attr('id', 'controls' + (i + 1))
          .html('<a class=\"non-current\" data-id='+ i +' href=\"javascript:void(0);\">'+ (i + 1) +'</a>')
          .appendTo($(".slider-controller"))
          .click(function(){              
            animateStuff($("a", $(this)).attr('data-id'), true);
          });                         
    } 

    // Set class for current item
    function setCurrent(i){
        i = parseInt(i)+1;
        $("li a", ".slider-controller").removeClass("current");
        $("li#" + "controls" + i + " a").addClass("current");
    };
    // Minor adjustment to container
    function adjust(){
          if(t > ts) t = 0;   
          if(t < 0) t = ts; 
          
          $("ul",target).css("margin-left",(t * width * -1)+"px");
          
          clickable = true;
          setCurrent(t);
    };

    // Animate images
    function animateStuff(dir, clicked){
      if (clickable == true){
          clickable = false;
          var ot = t; 

          switch(dir){
            case "next":
              t = t+1;           
              break; 
            case "prev":
              t = t-1;
              break; 
            case "first":
              t = 0;
              break; 
            case "last":
              t = ts;
              break; 
            default:
              t = dir;
              break; 
          }

          var diff = Math.abs(ot-t);
          var speed = diff * duration;  
          var p = (t * width * -1);

          $("ul", target).animate(
            { marginLeft: p+"px" }, 
            { queue:false, duration:duration, complete:adjust }
          );        
               
          /*
          if(clicked) clearTimeout(timeout);
          if(dir=="next" && !clicked){;
            timeout = setTimeout(function(){
              animateStuff("next", false);
            }, diff * duration + waiting);
          };
          */
      
        };
    };
  }
  
  

  
  
  
}));