// IIFE - Immediately Invoked Function Expression
(function(_fct){
    // The global jQuery object is passed as a parameter
  	_fct(window.jQuery, window, document);

}(function($, window, document) {

// The $ is now locally scoped 
// Listen for the jQuery ready event on the document
  $(function(){
    
    // The DOM is ready!
    console.log("DOM is ready");

    // Scroll
    /*
    var list = $("#navlist li a");

    list.each(function(){
      $(this).click(function(){
        var id = $(this).attr('href');
        $('html, body').animate({
          scrollTop: $(id).offset().top
        }, 2000);

       // alert($(this).attr('href'));
      });
    });
    */
    // Optimized with event delegation
    var list = $("#navlist");

    list.on("click", "li a", function(){
      var id = $(this).attr('href');
      $('html, body').animate({
        scrollTop: $(id).offset().top
      }, 800);
    });

  });  

  // DOM isn't ready yet !
  console.log("DOM isn't ready");
  
}));