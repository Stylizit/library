// IIFE - Immediately Invoked Function Expression
(function(_fct){
    // The global jQuery object is passed as a parameter
  	_fct(window.jQuery, window, document);

}(function($, window, document) {

// The $ is now locally scoped 
// Listen for the jQuery ready event on the document
  $(function(){
    
    // The DOM is ready!
    console.log("DOM is ready");

    // Only call functions we need when DOM is ready
    //checkMediaQueries();
    
  });  

  // DOM isn't ready yet !
  console.log("DOM isn't ready");

  // Caching variables
  function checkMediaQueries(){
    var widthCheck = window.matchMedia('screen and (min-width: 625px) and (max-width: 1024px)');
    widthCheck.addListener(setGrid);
    
    
  }
  function setGrid(mediaQueryList){
    console.log(mediaQueryList.matches);
    if(mediaQueryList.matches){
      set2COLGrid();
    }
    else{

     }
  }
 
  function set2COLGrid(){
    var target = $("#library");
    var items = $(target).find('a.item-expand');
    var tmpLibrary = "<div class=\" section group\">";

    $.each(items, function(i, item){
      tmpLibrary += item.innerHTML;
      if(i % 2 -1 == 0){
        tmpLibrary += "</div><div class=\" section group\">";
      }
      else{

      }
    });

    tmpLibrary += "</div>";
   
     // Algorithm : 
    // If the next tag following the 2nd 'a.item-expand' item is an 'a.item-expand'
    //    Add </div><div class="section group"> to the temp tab
    // Else
    //    
    $(target).html(tmpLibrary);
  }

  function set3COLGrid(){
    var target = $("#library");
    var items = $(target).find('a.item-expand');
    var tmpLibrary = "<div class=\" section group\">";

    $.each(items, function(i, item){
      tmpLibrary += item.innerHTML;
      if(i % 3 - 2 == 0){
        tmpLibrary += "</div><div class=\" section group\">";
      }
      else{

      }
    });

    tmpLibrary += "</div>";
    $(target).html(tmpLibrary);
  }

  //window.addEventListener("DOMContentLoaded", setValues, false);

}));