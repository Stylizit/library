// IIFE - Immediately Invoked Function Expression
(function(_fct){
    // The global jQuery object is passed as a parameter
  	_fct(window.jQuery, window, document);

}(function($, window, document) {

// The $ is now locally scoped 
// Listen for the jQuery ready event on the document
  $(function(){
    
    // The DOM is ready!
    console.log("DOM is ready");

    // Only call functions we need when DOM is ready
    expandDiv();
    
    //////////////
    // TODO :
    // - Resize header width on scroll-x event when window.size() < div
    // - ENTER PRODUCTION MODE -> Rearchitecture functions with proper variable caching 
    // (because this is really ugly code)
    /////////////

  });

  // DOM isn't ready yet !
  console.log("DOM isn't ready");

  function expandDiv(){
    // Caching variables
    var windows_width = $(window).width();
    var div_width = 980;
    var duration = 500;

    $(".item-expand:not(.add-triangle button)").on({
      "click": function(){
        
        // Caching variables
        var current = $(this);
        var childDiv = $(current).children(":first");
        var container = $(current).next();
        var offset_top = $(childDiv).offset().top;
        var offset_left = $(childDiv).offset().left;

        //console.log("top: "+offset_top+", left: "+offset_left);
        
        // Fade in the blurry expandable-container
        $(container).show().animate({
          opacity: "1"     
        }, {duration: duration, queue: false});
        
        /*$('.item-expand').next().fadeIn({duration: 100, queue: false});*/
        //console.log("div width: "+div_width+", win width: "+windows_width);

        // First position the expandable-popup so it seems to come from the item 
        $(container).children(":first").css({
          top: offset_top,
          left: offset_left
        });

        console.log("win width: "+windows_width+", div width: "+div_width+", left: "+(windows_width - div_width)*0.5);
        // Now animate/bring in the expandable-popup
        var left;
        if(windows_width > div_width){
          left = (windows_width - div_width) * 0.5;
        }
        else{
          left = 0;
        }
        $(container).children(":first").animate({
          left: left+"px",
          top: "0",
          width: div_width+"px",
          height: "850px"
        }, {duration: duration, queue: false});
       
        // Handle clicks outside the popup
        handleOutside(offset_top, offset_left);
        // Hande window resizing
        handleResize(offset_top, offset_left);
      }
    });
    // Block event for adding app <button> on right bottom corner 
    $(".add-triangle button").click(function(e) {
        e.stopPropagation();
    });

    function handleResize(top, left){

      // Caching variables
      var container = $(".expandable-container");
      var popup = $(".expandable-popup");
      var cross = $(".close-button");

      $(window).on('resize',function(){
        // Fix :fixed bug on header
        $("#header").css("width", $(window).width());
        // Adjust popup centering
        var left;
        if($(window).width() > div_width){
          left = ($(window).width() - div_width) * 0.5;
        }
        else{
          left = 0;
        }

        $(popup).css({
          left: left
        });
      });
    };

    function handleOutside(top, left){    
    /*
        $(".expandable-container").click(function(){
          $(".expandable-popup").animate({
            top: offset_top,
            left: offset_left,
            width: "0",
            height: "0"
          }, {duration: 500, queue: false});
          $(".expandable-container").fadeOut({duration: 500, queue: false});
        });
      */

      // Caching variables
      var container = $(".expandable-container");
      var popup = $(".expandable-popup");
      var cross = $(".close-button");

      // Hide popup when clicking outside or on the cross
      $(".expandable-container, .close-button").click(function(){
        $(popup).animate({
          top: top,
          left: left,
          width: "0",
          height: "0"
        }, {duration: duration, queue: false});
        $(container).fadeOut({duration: duration, queue: false});

        $(container).animate({
          opacity: "0"     
        }, {duration: duration, queue: false});
      });

      // Prevent events from getting pass expandable-popup
      $(".expandable-popup").click(function(e){
        e.stopPropagation();
      });
    };
  }
  
  
  
}));