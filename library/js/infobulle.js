// IIFE - Immediately Invoked Function Expression
(function(_fct){
    // The global jQuery object is passed as a parameter
  	_fct(window.jQuery, window, document);

}(function($, window, document) {

// The $ is now locally scoped 
// Listen for the jQuery ready event on the document
  $(function(){
    
    // The DOM is ready!
    console.log("DOM is ready");

    // Caching variables
    $('.infobulle').on({
      "mouseover": function(){
        var current = $(this);
        if($(current).attr("title") == ""){
          return false;
        }

        $("body").append("<span class=\" bubble\"></span>");
        var bubble = $('.bubble:last');
        bubble.append($(current).attr("title"));
        var top = $(current).offset().top;
        var left = $(current).offset().left + $(current).width() * 0.5;
        
        bubble.css({
          top: top,
          left: left + 30,
          opacity: 0
        });

        bubble.animate({
          top: top,
          opacity: 0.99
        });
      },
      "mouseout": function(){
        var bubble = $('.bubble:last');
        $(this).attr("title", bubble.text());
        
        bubble.animate({
          left: bubble.offset().left + 10,
          opacity: 0
        }, 500, "linear", function(){
          bubble.remove();
        });
        
      }

   });
  
  });
  // DOM isn't ready yet !
  console.log("DOM isn't ready");
  
}));